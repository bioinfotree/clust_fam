# Copyright Michele Vidotto 2017 <michele.vidotto@gmail.com>

.ONESHELL:


###### gene annotation conversion

### v1->jgv
.META: v12jgv.tab
	2	V1 annotation
	1	jgv annotation

v12jgv.tab:
	wget -qO - http://genomes.cribi.unipd.it/DATA/Gene_V1_file_conversion.txt \
	| select_columns 1 3 >$@


### v2->v1
.META: v2_2_v1.tab
	1	V2 annotation
	2	V1 annotation
	
# from Excel File: http://genomes.cribi.unipd.it/DATA/Matching_between_V1_V2.xls converted to TSV
v2_2_v1.tab:
	unhead Matching_between_V1_V2.txt \
	| sed 's/^[ \t]*//;s/[ \t]*$$//' \
	| tr -s ' \t' ';' \
	| sed 's/;/\t/' \
	| expandsets 2 >$@


### v1->v0
.META: v1_2_v0.tab
	1	Unknown
	2	jgv annotation
	3	V0 annotation
	4	V1 annotation
	5	Unknown

v1_2_v0.tab:
	wget -qO - http://genomes.cribi.unipd.it/DATA/gene_conversion_V1_V0.txt \
	| select_columns 4 3 >$@


### reconciliation of the 4 annotations v2->v1->v0->jgv.tab
.META: v2_v1_v0_jgv.tab
	1	V2 annotation
	2	V1 annotation
	3	V0 annotation
	4	jgv annotation

v2_v1_v0_jgv.tab: v12jgv.tab v2_2_v1.tab v1_2_v0.tab
	translate -a $< 2 <$^2 \
	| translate -a $^3 2 \
	| sed '1s/^/V2\tV1\tV0\tjgv\n/' >$@
	
######

stilbene_synthase.gff3.gz:
	$(call load_modules); \
	wget -qO - https://urgi.versailles.inra.fr/content/download/5183/39323/file/STS_CHS_on_v2.gff3 \
	| sed -e 's/cchr16/chr16/' \
	| gt gff3 -tidy -addids no -sort -gzip -o $@

terpene_synthase.gff3.gz:
	$(call load_modules); \
	wget -qO - https://urgi.versailles.inra.fr/content/download/5467/41152/file/VviTPS_20102016_Vitis_12X_2.gff3 \
	| gt gff3 -tidy -addids no -sort -gzip -o $@


### search for stilbene synthase into the projections of the scaffolds to the reference
stilbene_synthase.tab: assembly.bed stilbene_synthase.gff3.gz
	$(call load_modules); \
	if [ -e "$^2" ]; then \
		bedtools intersect \
		-bed \   * write output as BED, not bam *
		-wo \
		-a <(zcat $^2 | gff2bed | sortBed -i stdin) \
		-b <(sortBed -i $<) \
		| sortBed -i stdin >$@; \
	else
		touch $@
	fi

### search for terpene synthase into the projections of the scaffolds to the reference
terpene_synthase.tab: assembly.bed terpene_synthase.gff3.gz
	$(call load_modules); \
	if [ -e "$^2" ]; then \
		bedtools intersect \
		-bed \   * write output as BED, not bam *
		-wo \
		-a <(zcat $^2 | gff2bed | sortBed -i stdin) \
		-b <(sortBed -i $<) \
		| sortBed -i stdin >$@; \
	else
		touch $@
	fi





.PHONY: test
test:
	@echo 


ALL += v2_v1_v0_jgv.tab \
	stilbene_synthase.gff3.gz \
	terpene_synthase.gff3.gz


INTERMEDIATE += 

CLEAN += 